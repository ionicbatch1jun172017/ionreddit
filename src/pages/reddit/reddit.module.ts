import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RedditPage } from './reddit';
import { RedditService } from "./service/reddit.service";

@NgModule({
  declarations: [
    RedditPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicPageModule.forChild(RedditPage),
  ],
  exports: [
    RedditPage
  ],
  providers: [
    RedditService
  ]
})
export class RedditPageModule {}
